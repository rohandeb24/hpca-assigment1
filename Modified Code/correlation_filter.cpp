#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(float *image, int height, int width, float *filter, int filters)
{
    int b = 512;
    for(int i = 0; i < filters; ++i)
    {
        float *copy = new float[height * width];
        for(int hh=0;hh<height;hh=hh+b){
		for(int ww=0;ww<width;ww=ww+b){
			int h_min=height;
			if(hh+b<height) h_min=hh+b;
			for(int h = hh; h < h_min; ++h)
			{
			    int w_min=width;
			    if(ww+b<width) w_min=ww+b;
			    for(int w = ww; w < w_min; ++w)
			    {
				copy[h * width + w] = 0;
				__builtin_prefetch (&copy[h * width + w+b], 0, 0);

				for(int offy = -1; offy <= 1; ++offy)
				{

				    for(int offx = -1; offx <= 1; ++offx)
				    {
				        if(offx + h < 0 || offx + h >= height || offy + w < 0 || offy + w >= width)
				            continue;
					__builtin_prefetch (&image[(offx + h) * width + offy + w+b], 0, 0);
				        copy[h * width + w] += image[(offx + h) * width + offy + w] * 
				            filter[i * FILTER_SIZE * FILTER_SIZE + (1 + offx) * FILTER_SIZE + 1 + offy];
				    }
				}
			    }
			}
		}
        }
	for(int h = 0; h < height; ++h)
        {
            for(int w = 0; w < width; ++w){
		__builtin_prefetch (&image[h * width + w+100], 0, 0);
		__builtin_prefetch (&copy[h * width + w+100], 0, 0);
                image[h * width + w] = copy[h * width + w];}
        }
        delete copy;
    }

}

int main()
{
    int ht, wd;
    cin >> ht >> wd;
    float *img = new float[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    float *filter = new float[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("output.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
